# CMS - Content Management System.

## Pràctica 1: 

Activitat d’anàlisi comparatiu de diferents CMS’s. (Feta a classe).

## Pràctica 2: 

Seleccioneu una empresa real que tingui similitud amb algun dels tres casos que heu treballat a la pràctica de cerca d'informació (Restaurant, Botiga online, Startup de projectes).

Quina empresa heu seleccionat?

* Jofre: SquareEnix (Squaresoft)

* Kilian: Steam

* Uiliam: Blizzard

* Santi: Cooler Master

* Dani: McDonalds

* Manu: VelscoTattoo

Un cop seleccioneu la marca/empresa podeu instal·lar Wordpress i començar a personalitzar la web amb les opcions que heu investigat (plugins).

**Obligatori:** Mostrar captura i/o explicació. 

* Canviar el Tema per defecte instal·lant-ne un.
![Alt](Images/joomla1.png)
(Només en té 2, i per defecte).

* Instal·lar 5 plugins (justificant perquè els instal·leu).
![Alt](Images/joomla2.png)
(No es pot).

* Crear un perfil de cada tipus que permeti Wordpress. Explicar què pot fer cada tipus de perfil.
![Alt](Images/joomla3.png)
Public: Només pot llegir.
Guest: Només pot llegir.
Registered: Pot moure entrades, llegir-les, registrar-se, etc.
Special: Pot escriure, esborrar, modificar, publicar, llegir, registrar-se, etc.
Super User: Creadors i administradors totals del Joomla (nosaltres), tenim accés a tot i podem fer-ho tot.

* Canvia o afegeix algun canvi CSS a un Tema. Com ho has fet?
![Alt](Images/joomla4.png)
(No en té).

## Pràctica 3:

Feu la mateixa pàgina que teniu en Wordpress amb Joomla!

Tens un tutorial de com instal·lar Joomla [aqui](https://dungeonofbits.com/instalacion-de-joomla-en-linux.html).


## Pràctica 4: 

Creació d'un lloc Web amb Wordpress fet per una empresa real en grups de 2 persones.

* Estudi inicial (a classe).
* Visita a l'empresa (horari extraescolar o escolar, segons projecte).
* Creació d'una pàgina web amb wordpress personalitzada que s'ajusti al projecte i els objectius perseguits pel client.

