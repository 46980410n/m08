## CMS - Content Management System.

Seleccioneu una empresa real que tingui similitud amb algun dels tres casos que heu treballat a la pràctica de cerca d'informació (Restaurant, Botiga online, Startup de projectes).

Quina empresa heu seleccionat?

* Jofre: SquareEnix (Squaresoft)

* Kilian: Steam

* Uiliam: Blizzard

* Santi: Cooler Master

* Dani: McDonalds

* Manu: VelscoTattoo

Un cop seleccioneu la marca/empresa podeu instal·lar Wordpress i començar a personalitzar la web amb les opcions que heu investigat (plugins).

**Obligatori:** Mostrar captura i/o explicació. 


* Canviar el Tema per defecte instal·lant-ne un.
![Alt](Images/cms1.png)
He triat aquest tema, perquè la aparença és la més pròxima a la web original; tot i que no em deixa distribuir igual les imatges. 

* Instal·lar 5 plugins (justificant perquè els instal·leu).
![Alt](Images/cms2.png)
Bàsicament he triat el pack sencer de WooCommerce. En part, pel mateix motiu pel qual triaria Wordpress per aquesta feina: 
és el més extés, per tant el menys probable que quedi obsolet,
disposarà de més ajudes tan d'atenció al usuari com d'altres usuaris,
més actualtizacions,
més segur,
més fiable,
i al triar-ne el pack, les diferents opcions que tenen, son menys probables d'haver problemes de compatibilitat.

* Crear un perfil de cada tipus que permeti Wordpress. Explicar què pot fer cada tipus de perfil. 
![Alt](Images/cms3.png)

- Super-Admin.: És el creador de la pàgina i qui té la màxima autoritat i control en el blog o xarxa de blogs (jo). 
- Administrador: És qui té accés a tot els controls de la pàgina. 
- Gestor de tienda: És qui pot editar la botiga, però no les entrades. 
- Cliente: És qui compra a la nostra pàgina. 
- Subscriptor: És qui només pot editar el seu perfil. 
- Colaborador: És qui pot escriure i editar només les seves propies entrades, però no publicar-les. 
- Autor: És qui pot publicar i editar només les seves propies entrades. 
- Editor: És qui pot publicar i editar entrades, propies o d'altres usuaris. 

* Canvia o afegeix algun canvi CSS a un Tema. Com ho has fet?
![Alt](Images/cms4.png)
He personalitzat els colors, pels corporatius (blanc, negre i vermell). 



