En aquesta nova pràctica explorarem diferents **Sistemes Operatius** online que tenen funcionalitats similars als que poden oferir els sistemes operatius d'escriptori.

És important:
- Explorar a Internet qualsevol punt que no sabem com funciona.
- Per a cada punt anar redactant l'informe que el professor avaluarà.
- Els punts en negreta són obligatoris que estiguin en l'informe.

Escull una de les següents activitats i documenta-la degudament en l’informe:
- [**On Works.(Jordi)**](https://www.onworks.net/os-distributions/debian-based/free-zorinos-online) Fes un compte dins de AWS Amazon i analitza quines possibilitats ofereix a nivell d’infraestructura i serveis.
- [**Oodesk. (Uiliam)**](http://www.oodesk.com/) Sistema operatiu Online. Fes un compte i prova totes les funcionalitats que té a través de la web. Horbito
- [**SilveOS. (Dani)**](http://www.silveos.com/) Sistema Operatiu habilitat només per a Microsoft Explorer i derivats. Gràficament molt semblant a les versions de Windows
- [**Chrome OS.(Jofre)**](http://getchrome.eu/) Instal·la i configura Chrome OS en una màquina virtual i avalua quines són les possibilitats que té.
- [**Dissident.ai.(Santi)**](https://www.dissident.ai/) És un entorn que permet la gestió de diferents xarxes socials mitjançant una interfície i un menú.

**Extra Point.**  
- [**AWS Amazon.**](https://aws.amazon.com/es/) Fes un compte dins de AWS Amazon i analitza quines possibilitats ofereix a nivell d’infraestructura i serveis.
- [**OwnCloud.**](https://owncloud.org/) Instal·lar usant una versió en docker aquest SO i mira quines funcionalitats té. [Owncloud docker](https://doc.owncloud.org/server/latest/admin_manual/installation/docker/)
 
---



## Què SÍ es pot fer amb CHROME OS?

- __Productivitat__: S'hi poden fer servir tant les apps ofimàtiques de Google (Gmail, Drive, processador de text, fulls de càlcul, etc.), com altres tals com Trello, Asana, etc.

- __Xarxes socials__: Podem instalar sens problema Twitter, Facebook, Facebook Messenger, Instagram...

- __Jocs__: Soporta prous jocs, potser no els últims en sortir, pero World of Tanks i similars, sí. 

- __Series, pel·lícules i música__: També tenim accès a Netflix i Spotify.

En general, la majoria d'aplicacions de la Play Store són compatibles amb aquest O.S. ...però quines no?

![Alt](/Images/chromeOS.png "Title")

## Què NO es pot fer amb CHROME OS?

Tota aplicació que __requereixi associar un número de telèfon, no és compatible__ amb Chrome OS (com WhatsApp o Snapchat). 
També cal apuntar que tota app que descarreguem aquí, serà en la seva versió Mobile.



## Conclusió
Aquest O.S. és una molt bona alternativa per algun terminal petit, lleuger i portàtil.
Un exemple seria una tablet que t'enduries en un viatge llarg en avió o tren.