## P3. Eines de Gestió de Grups 

### Gestió de grups

Durant 4 hores treballarem P3. Explorarem diferents eines per a la gestió de grups i l'organització de xarxes socials. 

Tractarem diferents aplicatius. Per cada un d'ells s'ha d'introduir en l'informe.

**És important:**
> + Explorar a Internet qualsevol punt que no sabem com funciona.<br>
> + Per a cada punt anar redactant l'informe que el professor avaluarà.<br>


Comparació entre  diferents organitzadors de grup/tasques

1. Fes un compte en **Asana, Trello i Redbooth** i realitza un testeig de TOTES les funcionalitats que hi vegis. Acte seguit realitza un petit informe amb avantatges i inconvenients que li trobes a cada aplicatiu web.
-L'asana el veig més com una agenda, que com un "organitzador de grups" per sé. 
-El Trello, és el que estic més acostumat per les pràctiques, i em resulta més eficient, tant a l'hora d'organitzar grups en l'àmbit personal, com projectes o feines repetitives (o no) a nivell empresarial.
-Redbooth el més "pobre" a nivell gràfic, no resulta tan còmode de veure, però sí en l'efectivitat: es poden tenir converses, o pujar fotos... és molt semblant a una xarxa social privada, gairebé.

2. **Doodle.** Analitza aquest aplicatiu web i simula una quedada d'una reunió de la teva organització amb uns possibles proveïdors o clients. CAPTURES i explicació en l'informe.

    ![Alt](/Images/doodle01.png "Title")
    Primer de tot, ens demana lo més bàsic i esencial: Nom i localització de la reunió (també ens deixa escriure una nota, a mode de descripció, m'imagino; res obligatori)
    
    ![Alt](/Images/doodle02.png "Title")
    Després, ens insta a triar la/les data/es. 
    
    ![Alt](/Images/doodle03.png "Title")
    Aquí, quina mena de vot poden efectuar els convidats: 1 vot únic, un vot múltiple (potser tenen diversos dies disponibles)
    
    ![Alt](/Images/doodle04.png "Title")
    i finalment, ens diu a qui convidem.
    

3. **Gantt Project.** En entorns on s'ha de planificar un projecte és molt útil i molt comú utilitzar una eina que ajudi a veure en un diagrama com s'organitzen les tasques, qui és el responsable i en quin temps s'han de dur a terme. Realitza un diagrama de gantt amb aquesta eina que et serveixi per la teva organització. Pots realitzar un diagrama de Gantt del teu projecte de síntesi, per exemple (de setembre a juny) amb la previsió de les tasques, els tempos i els recursos que estimeu a dia d'avui destinar-hi. 
    
    Per instal·lar-ho descarregueu el fitxer (.deb) i executeu al terminal:
    
* [ ]     >  sudo dpkg -i paquete.deb
   
* [ ]      >  sudo apt-get -f install 
      
* [ ]   >  sudo dpkg -i paquete.deb

    ![Alt](/Images/gantt01.png "Title")
    Aquí, un exemple d'ús del Gantt Project.
    
    ![Alt](/Images/gantt02.png "Title")
    Aquí ens demana quin domini del projecte.
    
    ![Alt](/Images/gantt03.png "Title")
    Aquí, configurem els caps de setmana i temps del projecte. 


4. **MediaWiki.** En tota organització és necessari una wiki per a que tots els treballadors coneguin i modifiquin els punts comuns. Instal·la i configura MediaWiki i fes una pàgina amb les normes bàsiques de la teva organització. CAPTURES i explicació del que es va fent, guia't pels links d'ajuda que hi ha en el Moodle.

    ![Alt](/Images/wiki02.png "Title")
    Obrim el buscador, escribim "http://localhost/mediawiki" i se'ns obrirà això. (Obviament, després de descarregar el mediawiki).
    
    ![Alt](/Images/wiki03.png "Title")
    Tal i com indica el manual, ens apareix la Comprovació de l'entorn, per veure si disposem o no dels elements necessaris per la instalació del propi Mediawiki. 
    
    ![Alt](/Images/wiki04.png "Title")
    Seguidament, ens demana quina BBDD volem connectar (y la seva contraseña), com s'anomena i el nom d'usuari de la mateixa BBDD.
    
    ![Alt](/Images/wiki06.png "Title")
    Finalment, abans d'acabar ens demana (obviament) el nom d'usuari que Administrarà el Mediawiki, una contrasenya, etc.
    
    ![Alt](/Images/wiki05.png "Title")
    MENCIÓ ESPECIAL a aquesta opció!

En qualsevol moment dins l'informe podeu incloure qualsevol contingut web significatiu com poden ser imatges o vídeos. 
    
    
# Aquesta part la deixem PENDENT de moment.

### Eines xarxes socials

Explorarem ara diferents eines per a la gestió de les nostres **xarxes socials.**

Cal doncs, centrar-nos en els següents aplicatius analitzadors de xarxa social i descriure en l'informe com pot afectar en la nostra organització.

**És important:**
> + Explorar a Internet qualsevol punt que no sabem com funciona.
> + Per a cada punt anar redactant l'informe que el professor avaluarà.


Escull dos aplicatius i respon:
+ Analitza les funcionalitats que permet cada eina i anota-ho en l'informe. 
    
    Aplicatiu | Funcionalitats
    ---|---
    aplicatiu1 | funcionalitat 1 <br> funcionalitat 2 <br> funcionalitat3
    aplicatiu2 | funcionalitat 1 <br> funcionalitat 2

+ Quines xarxes socials pots controlar?
	
+ Què et permet fer per a cada xarxa social?

+ Per a què li pot interessar a la teva empresa/organització l'ús d'aquest aplicatiu?



Realitza **CAPTURES** del seu funcionament i un **LListat** de les seves funcionalitats principals.

Escull dos Aplicatius entre els següents o els links del moodle:

+ Tweetdeck
+ Hootsuite
+ HowSociable
+ Buffer.com
