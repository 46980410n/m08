## UF1. Ofimàtica i eines web

### P1. Correu electrònic i Calendari Web


Heu de seguir els passos següents per tenir configurat correu electrònic i calendari web per la vostra empresa. 

Us heu d’inventar una empresa i a partir d’aquí en configureu un correu electrònic i un calendari web. 

### (Per cada punt que aparegui (*) fes una captura de pantalla i on posa (COMPLETA) has d'explicar com ho has fet)

Abans de començar contesta:

**1. Nom de l’empresa:**
Gengaryne

<br><br>**2. A què es dedica la teva empresa?**
Entrenament de pokémon, especialitzats en tipo Fantasma

> <br><br>És important:
> + Explorar a Internet qualsevol punt que no sabem com funciona.
> + Per a cada punt anar redactant l'informe que el professor avaluarà.
> + Els punts en vermell són obligatoris que estiguin en l'informe.

<br><br>
#### Sobre Correu electrònic
1. Crea els comptes de correu necessaris per la teva empresa (Per exemple en Gmail).
2. Personalitza el tema. (*)
    ![Alt](/Images/fondo_gmail.png "Title")
3. Introdueix una foto en el teu perfil (Configuració - General).
4. Arxiveu un correu i recupereu-lo. (*)
    ![Alt](/Images/_Arxivat_.png "Title")
    ![Alt](/Images/no-arxivat.png "Title")
5. Envieu un missatge a la paperera i mostreu-lo. (*)
    ![Alt](/Images/paperera.png "Title")
6. Mostreu la icona per marcar un correu com a brossa.
7. Utilitzeu el cercador de correus per a localitzar un determinat correu. (*)
    ![Alt](/Images/buscador_correu.png "Title")
8. Moveu algun missatge per les pestanyes. 
9. Destaqueu alguns correus electrònics per poder trobar-los més endavant. (*)
    ![Alt](/Images/star.png "Title")
10. Comprova que l'adreça dels correus enviats s'han afegit als teus contactes de gmail. 
11. Comproveu l'ortografia d'algun missatge.
12. Crea etiquetes per a la vostra organització i assigna-hi un color diferent. (*)
    ![Alt](/Images/etiquetes.png "Title")
13. Activa i comprova el funcionament de la resposta automàtica amb un company. (*)
    ![Alt](/Images/resposta01.png "Title")
    ![Alt](/Images/resposta02.png "Title")
14. Activeu la funció Desfés l'enviament i establiu el temps que voleu que Gmail s'esperi a enviar el correu electrònic. Comprova el seu funcionament. (*)
    ![Alt](/Images/desfes.png "Title")
15. Configura la teva signatura. Per exemple, amb el teu nom complert i a sota una frase indicant la posició que ocupes dins l'organització. (*)
    ![Alt](/Images/signatura.png "Title")
16. Crea els Filtres respectius per a que els correus dels companys i professor se'ls assigni l'etiqueta respectiva. (*)
    ![Alt](/Images/filtres.png "Title")
17. Envia un correu als teus companys de grup i al professor per comprovar el funcionament del filtre. (*)
    ![Alt](/Images/filtres02.png "Title")
18. Proveu els operadors de cerca avançada. (*)
    ![Alt](/Images/avançada.png "Title")
19. Per a què serveix filtrar els missatges amb àlies de correu electrònic? (Contesta)
    -Per agilitzar la seva cerca, a més de diferenciar persones que s'anomenin igual/semblant.
20. Per a què serveix silenciar les converses?(Contesta)
    -Per evitar la disperció d'atenció, o per bloquejar-ne la comunicació amb aquella/es persona/es
21. Mostra el botó "Envia i arxiva". Per a què serveix? (Contesta)
    -![Alt](/Images/envia_i_arxiva.png "Title") Com el nom indica per enviar una resposta i archivar-la al mateix temps.

#### Calendari web

#####  A. Configuració de Google Calendar

1. Canvia el format de la data a dia/mes/any. (*)
    ![Alt](/Images/data.png "Title")
2. Canvia el format d'hora a 24 h. 
    ![Alt](/Images/hora.png "Title")
3. Canvia el dia de començament de setmana a dilluns. (*)
    ![Alt](/Images/dilluns.png "Title")

#####  B. Events

##### Nota: Tots els events s'han de crear en el calendari de l'usuari.

1. Crea un event associat a un dia complert, per exemple un aniversari.(*)
    ![Alt](/Images/vaga.png "Title")
2. Fes que l'event de l'aniversari es repeteixi tots els anys.(Contesta)
3. Comprova com es visualitza l'event en les diferents vistes (dia, setmana, ..., agenda).
4. Comprova que l'event es mostra en el calendari de l'any vinent.(*)
    ![Alt](/Images/any_vinent.png "Title")
5. Comprova que pots desplaçar l'event, però que les repeticions no es desplacen simultàniament. Per fer-ho, cal tornar l'event a la seva data original, desactivar la repetició, moure de nou l'event a la data correcta i activar les repeticions. (**)
    ![Alt](/Images/canvi.png "Title")
6. Crea un event associat a un període de temps en un dia determinat, per exemple, l'assistència a una reunió de la teva organització, de 20:00 h. a 22:00 h.(*)
7. ![Alt](/Images/reunió2022.png "Title")
7. Edita l'event per rebre una notificació de l'event per correu (fes-ho de manera que la notificació es rebi en els propers 5 minuts).(*)
    ![Alt](/Images/tictoc.png "Title")
8. Canvia el color de l'event.
9. Comprova com es visualitza l'event en les diferents vistes (dia, setmana, ..., agenda).
10. Comprova que has rebut la notificació per correu de l'assistència de la reunió anterior.(*)
    ![Alt](/Images/tictoc2.png "Title")

#####  C. Tasques
1. En la vista mensual crea una tasca associada a un dia, per exemple comprar un regal per a una persona. 
2. Comprova que la tasca s'ha afegit en la llista de tasques disponible en Gmail (*)
    ![Alt](/Images/tasca.png "Title")

#####  D. Thunderbird  *(opcional)*
1. Instal·lació i administració del vostre compte de gmail des del client de correus Thunderbird

#####  E. Calendaris
1. Comprova que pots desactivar i activar la visibilitat dels calendaris fent clic en el quadre situat a l'esquerra del nom dels calendaris. (*)
    ![Alt](/Images/extres.png "Title")
2. Oculta el calendari Festius d'Espanya fent clic en el desplegable situat a la dreta del nom del calendari.
3. Restaura el calendari Festius d'Espanya fent clic en el desplegable situat a la dreta d'Altres calendaris, opció Configuració.
4. Afegeix un calendari fent clic en el desplegable situat a la dreta d'Altres calendaris, opció Explorar calendaris interessants. Per exemple, afegeix el calendari de partits d'un equip de l'esport que més t'agradi. (*)
    ![Alt](/Images/calendarium.png "Title")
5. El calendari Aniversaris que mostra Google Calendar es genera a partir de les dades dels contactes. Crea un segon calendari Aniversaris per poder afegir més aniversaris, fent clic en el desplegable situat a la dreta de Els meus calendaris, opció Crea nou calendari. (*)
    ![Alt](/Images/aniversaris.png "Title")
6. Afegeix a aquest calendari els aniversaris de la teva família més propera, creant events repetits i associant-los al calendari Aniversaris. Comprova que al desactivar i activar la visibilitat del calendari, s'oculten i es mostren els events. (*)
    ![Alt](/Images/teste.png "Title")

#####  F. Compartir calendaris  *(opcional)*
1. Crea un calendari amb el nom de la teva empresa. (*)
2. Comparteix aquest calendari amb el company de grup i amb el professor.(*)
3. Comprova que els calendaris que han compartit amb tu altres companys es mostren en l'apartat Altres calendaris.
4. Crea events en els calendaris del teus companys. Per exemple: entregues de productes, reunions, inauguracions, entrevistes de feina, etc.
5. Modifica el color dels calendaris d'acord amb algun criteri. Per exemple, que els calendaris d'organitzacions similars siguin d'un mateix color.
6. Comprova que els events creats en els calendaris es mostren en el calendari general.

#####  G. Gsuite
1. Escull 4 aplicacions que trobaràs en el Gsuite Marketplace de Gmail, instal·la’ls i prova’n el seu funcionament.  (*)
    


#####  H. Incrustar un calendari en una pàgina web *(opcional)*
Nota: S'ofereixen dues formes de publicar un calendari com a pàgina web. La primera es més senzilla, donat que és Google qui ens publica el calendari i crea l'adreça, però no es pot afegir informació addicional. La segona és més complexe, perquè creem nosaltres la pàgina web, incrustem el calendari i publiquem la pàgina, però podrem afegir qualsevol contingut.
1. En les opcions de configuració del calendari “nomOrganitzacio” > Compartir aquest calendari, marca la casella Fer públic aquest calendari.
2. En les opcions de configuració del del calendari “nomOrganitzacio”  > Detalls del calendari > Adreça del calendari, fes clic en el botó blau HTML para veure l'adreça pública del calendari.
3. Fes clic en l'adreça i comprova que es veu el calendari. Guarda com a marcador l'adreça. Tanca la sessió de Google i comprova que el marcador funciona i pots consultar el calendari.

1. En un editor de text sense format, crea un document de text anomenat calendari.html.
2. En les opcions de configuració del calendari “nomOrganitzacio” > Detalls del calendari > Incrustar aquest calendari, copia el codi html (<iframe src=...></iframe>
3. Enganxa aquest codi en el document.
4. Completa el codi html:

```html
      <html>
            <h1>nomOrganització. Calendari dels events corresponents a la meva organització</h1>
            <iframe src ... ></src>
      </html>
```
      
5. Puja el document a Google Drive.
6. Comparteix el document (al compartir fes clic en Avançada (situat a sota a la dreta), s'obrirà la finestra de configuració d'accés compartit, escull l'opció Canviar situada a la dreta de privat, s'obrira la finestra ús compartit d'enllaços, escull Sí: públic en la web)
7. L'enllaç serà del tipus:
     `https://drive.google.com/open?id=XXXXXX&authuser=0`
8. L'enllaç que es pot enviar és el següent:
      `https://www.googledrive.com/host/XXXXXX`
      en el que XXXXX es l'ID del document.
