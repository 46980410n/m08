# P2. Eines Ofimàtiques Web

Durant 4 hores treballarem P2. 

Continuant l' informe iniciat en P1, ara toca incloure eines ofimàtiques web. 

S'han d'incloure els següents punts.

Per cada punt escollit s'ha d'introduir en l'informe igual que en P1.

> És important:
>> Organitzar-se amb el company per repartir-se la feina.
>> Explorar a Internet qualsevol punt que no sabem com funciona.
>> Per a cada punt anar redactant l'informe que el professor avaluarà.
>> Els punts en vermell són obligatoris que estiguin en l'informe.


### Comparació entre els diferents paquets ofimàtics web

1.Fes una **taula comparativa** entre els diferents sistemes i inclou-lo en l'informe. 

Indica quines eines té cada paquet ofimàtic web, apreciareu moltes diferències

+ Google Drive
+ Office 365
+ Zoho Docs
+ ThinkFree
+ Feng Office 

Exemple:

Aplicació | Google Drive | Office 365 | Zoho Docs | ThinkFree | Feng Office
---|---|---|---|---|---
Processador de text | sí | sí | sí | sí | sí
Fulla de càlcul | si | sí | sí | sí | sí
Presentacions | sí | sí | sí | sí | sí
correu | sí | sí | sí | sí | sí
Disseny | sí | no | no | no | no
BBDD | no | sí | sí | no | no
intercomunicació | sí | sí | sí | sí | sí
Núvol | sí | sí | sí | sí | sí
Gratuit | sí | no | no | sí | no
Cat/Esp | sí | sí | sí | no | no



---

2.Un cop vistes les eines que podeu usar:

Escull entre Zoho Docs, Office 365, Google Drive i Feng Office i explica la **majoria d'aplicacions** que conté el paquet, també les menys conegudes. (Llistat d'aplicacions i captura del seu funcionament) 

Intenteu que tingui relació amb la vostra empresa.


Trio Google Docs (triaria Microsoft Office, però sóc pobre i la meva empresa també).

**Aplicació:** GoogleDocs Writer

**Link:** [Click me!](https://docs.google.com/document/u/1/)

**Funcionament:**

![images](Images/googledocs01.png)

---

**Aplicació:** GoogleDocs Sheet

**Link:** [Click me!](https://docs.google.com/spreadsheets/u/0/)

**Funcionament:**

![images](Images/googledocs02.png)

---

**Aplicació:** GoogleDocs Slides

**Link:** [Click me!](https://docs.google.com/presentation/u/0/)

**Funcionament:**

![images](Images/googledocs03.png)

---

**Aplicació:** Jamboard

**Link:** [Click me!](https://jamboard.google.com/)

**Funcionament:**

![images](Images/googledocs04.png)

---


3.Tot seguit en el Google Drive creeu una carpeta de projecte on existeixi com a mínim dos documents col·laboratius, en PODEU POSAR MÉS:

**Link:** [Carta de presentació](https://docs.google.com/document/d/1CJ15IORARNEYf7gmxrU5YlYxGnYan7lgA3wmgTVs93s/edit) 

**Link:** [Pressupost simulat](https://docs.google.com/spreadsheets/d/1uypoRkDLuSWWNmXL-FqocCQ5lF_G9tUGdeJXKpHVbjw/edit#gid=0) 

**Link:** [Formulari de contacte web](https://docs.google.com/forms/d/1A4e2GL8Y3ccjhJXSKh4AikZRRURhVWJ2hhXkAfoqaKY/edit) 

(per fer-lo): **Link:** [Formulari de contacte web](https://forms.gle/itb2RhhMB4neWhVu6) 


#### Infogràfics

4.Easel.ly: Realitza un infogràfic útil per a la teva empresa. És important que es vegi en l'infogràfic la informació important remarcada, utilitza aquesta pàgina web, o una altra que hagis descobert que faci la mateixa funció. CAPTURES i explicació en l'informe.

5.Finalment mitjançant una eina com prezzi o slideshare fes una presentació que permeti veure en profunditat l'estructura de la teva organització.

En qualsevol moment dins l'informe podeu incloure qualsevol contingut web significatiu com poden ser imatges o vídeos.