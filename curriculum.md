## Jofre Cros 

![Alt](/Images/anti-tinder.jpeg)



**Nom**: Jofre Palacios Cros

**Adreça**: Av. Meridiana 28

**Telèfon**: 698 55 20 49

**Mail**: jofrecros@gmail.com


#### Actualment #### 

**Estuadiant**: 2n SMIX (tarda).

**Treballant**: Pràctiques de suport a l'usuari a: Telefónica Investigación y Desarrollo (TID).


#### Experiència laboral ####

Gestor d'usuaris de Rent & Ride. Chamonix, França.


#### Estudis ####

**Estudis finalitzats**: E.S.O. en IES Icària.
                         Curs Oficial de Tripulante de Cabina de Pasajeros.


#### Coneixements informàtics ####

* Certificat ![Linux Essentials](/Images/LE-1.pdf)
* Paquete LibreOffice nivel medio.
* Usuario nivel medio de Packet Tracer y Virtual Box.
* Instalación de Sistemas Operativos Linux y Windows.
* Conocimientos de montaje de ordenadores.



#### Capacitats transversals ####

- Responsable
- Dinàmic
- Lideratge
- Adaptable
- Sociable
- Autodidacte


##### Idiomes #####

**Català**: Natiu

**Castellà**: Natiu

**Anglès**: First Certificate (B2)

**Francès**: Iniciat


##### Hobbies #####

* Llegir
* Escriure
* Córrer
* Natació
* Música
* Sociabilitzar


#### Futurs i aspiracions ####

 - [x] Iniciar el meu aprenentatge en el món de la informàtica.
 - [ ] Apendre programació.
 - [ ] Coneixer i aprendre noves cultures i indrets.